#
# Cookbook Name:: chef-docker
# Spec:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

# The following are only examples, check out https://github.com/chef/inspec/tree/master/docs
# for everything you can do.

describe package('docker.io') do
  it { should be_installed }
end

describe service('docker') do
  it { should be_installed }
  it { should be_enabled }
#  it { should be_running }
end

describe yaml('.kitchen.yml') do
  its('driver.name') { should eq('docker') }
end
