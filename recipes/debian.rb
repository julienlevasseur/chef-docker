#
# Cookbook Name:: chef-docker
# Recipe:: debian
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

apt_update

%w(
  apt-transport-https
  ca-certificates
  curl
  software-properties-common
).each do |pkg|
  package pkg
end

apt_repository 'docker' do
  uri 'https://download.docker.com/linux/ubuntu'
  distribution 'bionic'
  components ['stable']
  key 'https://download.docker.com/linux/ubuntu/gpg'
end

apt_update

if node['platform'] == 'ubuntu'
  if node['platform_version'] == '16.04'
    package 'docker.io'
  elsif node['platform_version'] == '18.04'
    package 'docker-ce'
  end
end

service 'docker' do
  action [:enable, :start]
  not_if { Chef::Config[:file_cache_path].include?('kitchen') }
end
