#
# Cookbook:: docker
# Recipe:: default
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

include_recipe 'docker::debian' if platform_family?('debian')
include_recipe 'docker::centos' if platform_family?('centos')
